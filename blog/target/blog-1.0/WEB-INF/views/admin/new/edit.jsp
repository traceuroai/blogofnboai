<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/common/taglib.jsp"%>
<c:url var="newURL" value="/admin67/new/list?page=1&limit=3" />
<c:url var="eidtNewURL" value="/admin67/new/edit" />
<c:url var="newAPI" value="/admin67/new-api" />
<c:url var="uploadURL" value="/admin67/new-api-image" />
<html>
<head>
<title>Chỉnh sửa bài viết</title>
<base href="${pageContext.servletContext.contextPath}/">
</head>
<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
					try {
						ace.settings.check('breadcrumbs', 'fixed')
					} catch (e) {
					}
				</script>
				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a
						href="<c:url value="/admin67/home"/>">Trang chủ</a></li>
					<li class="active">Chỉnh sửa bài viết</li>
				</ul>
				<!-- /.breadcrumb -->
			</div>

			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${not empty message}">
							<div class="alert alert-${alert}">${message}</div>
						</c:if>
						<form:form id="formSubmit" modelAttribute="model">
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Thể
									loại</label>
								<div class="col-sm-9">
									<%-- <select class="form-control" id="categoryCode"
										name="categoryCode">
										<c:if test="${empty model.categoryCode}">
											<option value="">--- Chọn thể loại ---</option>
											<c:forEach var="item" items="${categories}">
												<option value="${item.code}">${item.name}</option>
											</c:forEach>
										</c:if>
										<c:if test="${not empty model.categoryCode}">
											<option value="">--- Chọn thể loại ---</option>
											<c:forEach var="item" items="${categories}">
												<option value="${item.code}"
													<c:if test="${item.code == model.categoryCode}">selected="selected"</c:if>>
													${item.name}</option>
											</c:forEach>
										</c:if>
									</select> --%>
									<%-- <form:select cssClass="form-control" path="categoryCode" id="categoryCode" items="${categorys}">
										<form:option value="" label="--- Chọn thể loại ---" />
										<form:options items="${categorys}" itemLabel="name" itemValue="code"/>
									</form:select> --%>
									<form:select path="categoryCode" id="categoryCode">
										<form:option value="" label="-- Chọn thể loại --" />
										<form:options items="${categories}" />
									</form:select>
								</div>
							</div>
							<br />
							<br />
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Tiêu
									đề</label>
								<div class="col-sm-9">
									<form:input cssClass="form-control" path="title" id="title" />

								</div>
							</div>
							<br />
							<br />
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Hình
									đại diện</label>
								<div class="col-sm-9">
									<input type="file" class="form-control" id="thumbnail"
										name="thumbnail" value="${model.thumbnail}" />
								</div>
							</div>
							<br />
							<br />
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Mô
									tả ngắn</label>
								<div class="col-sm-9">
									<textarea rows="" cols="" id="shortDescription"
										name="shortDescription" style="width: 820px; height: 175px">${model.shortDescription}</textarea>
								</div>
							</div>
							<br />
							<br />
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Nội
									dung</label>
								<div class="col-sm-9">
									<form:textarea rows="" cols="" id="content" name="content"
										path="content" style="width: 820px; height: 175px" />
								</div>
							</div>
							<br />
							<br />
							<br>
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Likes</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="likes" name="likes"
										value="${model.likes}" />
								</div>
							</div>
							<br />
							<br />
							<br>
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Views</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="views" name="views"
										value="${model.views}" />
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<c:if test="${not empty model.id}">
										<input type="button"
											class="btn btn-white btn-warning btn-bold"
											value="Cập nhật bài viết" id="btnAddOrUpdateNew" />
									</c:if>
									<c:if test="${empty model.id}">
										<input type="button"
											class="btn btn-white btn-warning btn-bold"
											value="Thêm bài viết" id="btnAddOrUpdateNew" />
									</c:if>
									<input type="button" class="btn btn-white btn-warning btn-bold"
										value="Hủy" id="btnReset" />
								</div>
							</div>
							<input type="hidden" value="${model.id}" id="id" name="id" />
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		var editor = '';
		$(document).ready(function() {
			// biến textarea thành 1 filed có thể sử dụng ckeditor được.
			editor = CKEDITOR.replace('content');
		});

		
//		var dat = '';
		// upload file
		 $(document).ready(function() {})
		 
		 function addImage(data) {
			 var dataArray = {};
			 var files = $('#thumbnail')[0].files[0];
			 	if (files != undefined) {
			 		var reader = new FileReader();
			 		reader.onload = function(e) {
			 			dataArray["base64"] = e.target.result;
			 			dataArray["name"] = files.name;
			 			dataArray["id"] = data.id;
			 			uploadFile(dataArray);
			 		};
			 		reader.readAsDataURL(files);
			 	}
		};
		 
		 /* $('#thumbnail').change(function() {
		 	var dataArray = {};
		 	var files = $(this)[0].files[0];
		 	if (files != undefined) {
		 		var reader = new FileReader();
		 		reader.onload = function(e) {
		 			dataArray["base64"] = e.target.result;
		 			dataArray["name"] = files.name;
		 			uploadFile(dataArray);
		 		};
		 		reader.readAsDataURL(files);
		 	}
		 }); */
		 
		function uploadFile(data) {
		 	$.ajax({
		 		url : '${uploadURL}',
		 		type : 'POST',
		 		data : JSON.stringify(data),
		 		contentType : 'application/json',
		 		dataType : 'json',
		 		success : function(res) {
		 			console.log(res);
		 			dat = res.name;
		 		},
		 		error : function(res) {
				 	console.log(res);
		 		}
		 	});
		 }
		//end - upload file
		
		
		
		$('#btnAddOrUpdateNew').click(function(e) {
			e.preventDefault();
			var data = {};
			var formData = $('#formSubmit').serializeArray();// lấy hết giá trị trong form thành 1 mảng theo thuộc tính name
			$.each(formData, function(i, v) {// v: lưu trữ name, value
				data["" + v.name + ""] = v.value;
			});
//			data["thumbnail"] = dat;
			data["content"] = editor.getData();
			var id = $('#id').val();
			if (id == "")// trường hợp thêm mới
			{
				addNew(data);
			} else {
				updateNew(data);
			}
		});

		function addNew(data) {
			$.ajax({
				url : '${newAPI}',// không được gọi trực tiếp (phải qua c:url) -> khai báo trên đầu
				type : 'POST',
				contentType : 'application/json',
				data : JSON.stringify(data),// dữ liệu gửi lên server (đã chuyển từ jsObject -> json)
				dataType : 'json',
				success : function(result) {
					addImage(result);
					window.location.href = "${eidtNewURL}?id=" + result.id
							+ "&message=insert_success";
				},
				error : function(error) {
					window.location.href = "${newURL}&message=error_system";
				}
			});
		}

		function updateNew(data) {
			$.ajax({
				url : '${newAPI}',
				type : 'PUT',
				contentType : 'application/json',
				data : JSON.stringify(data),
				dataType : 'json',
				success : function(result) {
					window.location.href = "${eidtNewURL}?id=" + result.id
							+ "&message=update_success";
				},
				error : function(error) {
					window.location.href = "${eidtNewURL}?id=" + result.id
							+ "&message=error_system";
				}
			});
		}
	</script>

</body>

</html>


