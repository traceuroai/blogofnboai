<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>


<div id="" class="section wb">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="message-box">
					<h2>I'm Oai.</h2>
					<p>
						Là 1 lập trình viên it tôi xây dựng blog một phần vì những lợi ích
						của việc viết blog đối với bản thân, ngoài ra tôi mong rằng các
						kiến thức của bản thân có thể giúp ích được một số bạn trẻ sẽ hoặc
						đang theo ngành it, dựa vào chia sẻ hiểu biết của cá nhân tôi sẽ
						ít nhiều có một cái nhìn đa chiều hơn của ngành it. </br> </br> Những kiến
						thức, kinh nghiệm được chia sẻ ở đây sẽ có thể không truyền đạt
						được hết ý hiểu hoặc có thể có sai sót trong quá trình tìm hiểu
						của cá nhân. </br> </br> Tôi luôn mong được sự góp ý, giúp đỡ của mọi người
						để những bài viết ngày càng chất lượng hơn và cùng nhau phát
						triển.
					</p>

					<!-- <a href="#" class="sim-btn btn-hover-new" data-text="Download CV"><span>Download
							CV</span></a> -->
				</div>
				<!-- end messagebox -->
			</div>
			<!-- end col -->


			<div class="col-md-6">
				<div class="right-box-pro wow fadeIn">
					<img src="<c:url value='/template/web/uploads/back2.jpg' />"
						alt="" class="img-fluid img-rounded">
				</div>
				<!-- end media -->
				<br> <br>
				<h2>Về Blog</h2>
				<ul>
					<li>Trang chủ : tổng quan về blog, tập hợp các bài viết</li>
					<li>IT & cuộc sống : những điều cần biết xoay quanh ngành it và đời sống cá nhân</li>
					<li>Kiến thức thú vị : chia sẻ những kiến hay về lập trình</li>
					<li>Tài liệu lập trình : Tài liệu học lập trình</li>
					<li>ABOUT ME : giới thiệu tổng quan về bản thân và mục tiêu blog</li>
				</ul>
			</div>
			<!-- end col -->
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="message-box">
					<h3>Mọi ý kiến góp ý xin liên hệ :</h3>
					<ul>
						<li><a href="https://www.facebook.com/oai.nguyenba.3"
							style="color: blue">Facebook</a></li>
						<li><a href="oaiker2k@gmail.com" style="color: blue">Gmail</a></li>
					</ul>
				</div>
				<!-- end messagebox -->
			</div>
		</div>
		<!-- end row -->

	</div>
	<!-- end container -->
	<h2 style="text-align: center;">Cám ơn các bạn đã ghé thăm !</h2>
</div>
<!-- end section -->