<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>

<!DOCTYPE html>
<html>
<head></head>
<body>

	<div id="" class="section lb">
		<div class="container">
			<div class="section-title text-left">
				<h3>${new1.title}</h3>
				<p>${new1.categoryCode}</p>
			</div>
			<!-- end title -->

			<div class="row">
				<div class="col-md-8 col-sm-8 col-lg-8">
					<div>${new1.content}</div>
				</div>
				<div class="col-md-4 col-sm-4 col-lg-4">
					<div style="padding-left: 50px;">
						<h3>Bài viết liên quan</h3>
						<br>
						<c:forEach var="item" items="${news}">
							<h4>
								<a href="<c:url value='/chi-tiet/${item.id}' />"
									title="Xem chi tiết">${item.title}</a>
							</h4>
							<p>${item.shortDescription}</p>
							<hr>
						</c:forEach>
					</div>
				</div>
			</div>
			<br> <br>
			<div class="row">
				<span style="margin-right: 20px; font-size: 20px;"><i
					class="fas fa-eye" style="margin-right: 7px; margin-left: 10px;">
						${new1.views} </i></span> <span style="margin-left: 20px; font-size: 20px;"><a
					href="#" title="Yêu thích"><i class="far fa-heart"></i></a>
					${new1.likes}</span>
			</div>

		</div>
	</div>
</body>
</html>

