<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>


<div id="" class="section wb">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="message-box">
					<h2>About</h2>
					<p>Là một Blog thông tin thể hiện cái nhìn chủ quan của tôi về
						lĩnh vực công nghệ thông tin nói chung, sở thích, chia sẻ cá nhân
						về những kiến thức thú vị trong ngành hoặc những trải nghiệm cá
						nhân xung quanh ngành it.</p>

					<a href="<c:url value='/ve-toi'/>" class="sim-btn btn-hover-new"
						data-text="Download CV"><span>Xem thêm</span></a>
				</div>
				<!-- end messagebox -->
			</div>
			<!-- end col -->

			<div class="col-md-6">
				<div class="right-box-pro wow fadeIn">
					<img src="<c:url value='/template/web/uploads/back2.jpg' />" alt=""
						class="img-fluid img-rounded">
				</div>
				<!-- end media -->
			</div>
			<!-- end col -->
		</div>
		<!-- end row -->
	</div>
	<!-- end container -->
</div>
<!-- end section -->

<div id="" class="section lb">
	<div class="container">
		<div class="section-title text-left">
			<h3>Các công nghệ sử dụng</h3>
			<p></p>
		</div>
		<!-- end title -->

		<div class="gallery-menu row">
			<div class="col-md-12">
				<div class="button-group filter-button-group text-left">
					<button class="active" data-filter="*">All</button>
					<button data-filter=".gal_a">Web Development</button>
				</div>
			</div>
		</div>

		<div class="gallery-list row">
			<div class="col-md-4 col-sm-6 gallery-grid gal_a">
				<div class="gallery-single fix">
					<img
						src="<c:url value='/template/web/uploads/gallery_img-01.jpg' />"
						class="img-fluid" alt="Image">
					<div class="img-overlay">
						<a
							href="<c:url value='/template/web/uploads/gallery_img-01.jpg' />"
							data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i
							class="fa fa-picture-o"></i></a>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-6 gallery-grid">
				<div class="gallery-single fix">
					<img
						src="<c:url value='/template/web/uploads/gallery_img-03.jpg' />"
						class="img-fluid" alt="Image">
					<div class="img-overlay">
						<a
							href="<c:url value='/template/web/uploads/gallery_img-03.jpg' />"
							data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i
							class="fa fa-picture-o"></i></a>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-6 gallery-grid gal_a">
				<div class="gallery-single fix">
					<img src="<c:url value='/template/web/uploads/spring.png' />"
						class="img-fluid" alt="Image">
					<div class="img-overlay">
						<a href="<c:url value='/template/web/uploads/spring.png' />"
							data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i
							class="fa fa-picture-o"></i></a>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-6 gallery-grid gal_a">
				<div class="gallery-single fix">
					<img src="<c:url value='/template/web/uploads/java.png' />"
						class="img-fluid" alt="Image">
					<div class="img-overlay">
						<a href="<c:url value='/template/web/uploads/java.png' />"
							data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i
							class="fa fa-picture-o"></i></a>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-6 gallery-grid gal_a">
				<div class="gallery-single fix">
					<img
						src="<c:url value='/template/web/uploads/gallery_img-04.jpg' />"
						class="img-fluid" alt="Image">
					<div class="img-overlay">
						<a
							href="<c:url value='/template/web/uploads/gallery_img-04.jpg' />"
							data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i
							class="fa fa-picture-o"></i></a>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-6 gallery-grid gal_a">
				<div class="gallery-single fix">
					<img src="<c:url value='/template/web/uploads/sql.jpg' />"
						class="img-fluid" alt="Image">
					<div class="img-overlay">
						<a href="<c:url value='/template/web/uploads/sql.jpg' />"
							data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i
							class="fa fa-picture-o"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="" class="section lb">
	<div class="container">
		<div class="section-title text-left">
			<h3>Blog</h3>
			<p></p>
		</div>
		<!-- end title -->

		<div class="row">
			<div class="col-md-4 col-sm-6 col-lg-4">
				<div class="post-box">
					<div class="post-thumb">
						<img src="<c:url value='/template/web/uploads/blog-01.jpg' />"
							class="img-fluid" alt="post-img" />
						<div class="date">
							<span>06</span> <span>Aug</span>
						</div>
					</div>
					<div class="post-info">
						<h4>
							<a href="" title="">Quisque auctor lectus interdum nisl
								accumsan venenatis.</a>
						</h4>
						<ul>
							<li>by admin</li>
							<li>Apr 21, 2018</li>
							<li><a href="#"><b> Comments</b></a></li>
						</ul>
						<p>Etiam materials ut mollis tellus, vel posuere nulla. Etiam
							sit amet massa sodales aliquam at eget quam. Integer ultricies et
							magna quis.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-lg-4">
				<div class="post-box">
					<div class="post-thumb">
						<img src="<c:url value='/template/web/uploads/blog-02.jpg' />"
							class="img-fluid" alt="post-img" />
						<div class="date">
							<span>06</span> <span>Aug</span>
						</div>
					</div>
					<div class="post-info">
						<h4>Quisque auctor lectus interdum nisl accumsan venenatis.</h4>
						<ul>
							<li>by admin</li>
							<li>Apr 21, 2018</li>
							<li><a href="#"><b> Comments</b></a></li>
						</ul>
						<p>kkkkk.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-lg-4">
				<div class="post-box">
					<div class="post-thumb">
						<img src="<c:url value='/template/web/uploads/blog-03.jpg' />"
							class="img-fluid" alt="post-img" />
						<div class="date">
							<span>06</span> <span>Aug</span>
						</div>
					</div>
					<div class="post-info">
						<h4>Quisque auctor lectus interdum nisl accumsan venenatis.</h4>
						<ul>
							<li>by admin</li>
							<li>Apr 21, 2018</li>
							<li><a href="#"><b> Comments</b></a></li>
						</ul>
						<p>Etiam materials ut mollis tellus, vel posuere nulla. Etiam
							sit amet massa sodales aliquam at eget quam. Integer ultricies et
							magna quis.</p>
					</div>
				</div>
			</div>
		</div>
		<br> <br>
		<div class="row">
			<c:forEach var="item" items="${news}">
				<div class="col-md-4 col-sm-6 col-lg-4">
					<div class="post-box">
						<div class="post-thumb">
							<img src="<c:url value='/template/web/uploads/blog-01.jpg' />"
								class="img-fluid" alt="post-img" />
							<div class="date" style="font-size: 10px;">
								<span>${item.createdDate}</span>
							</div>
						</div>
						<div class="post-info">
							<h4>
								<a href="<c:url value='/chi-tiet/${item.id}' />" title="Xem chi tiết">${item.title}</a>
							</h4>
							<ul>
								<li>by admin  </li>
								<%-- <li>${item.createdDate}</li> --%>
								<li><i class="fas fa-eye" style="margin-right: 7px; margin-left: 10px;"> ${item.views} </i>      </li>
								<li><i class="far fa-heart"></i>    ${item.likes}</li>
								<li><i class="fas fa-comment-slash">  </i></li>
								<!-- <li><b>Comments</b></li> -->
							</ul>
							<p>${item.shortDescription}</p>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>

	</div>
</div>