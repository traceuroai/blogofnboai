<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport"
	content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Site Metas -->
<title>BlogIT</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon"
	href="<c:url value='/template/web/images/title.png' />"
	type="image/x-icon" />
<link rel="apple-touch-icon"
	href="<c:url value='/template/web/images/apple-touch-icon.png' />">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="<c:url value='/template/web/css/bootstrap.min.css' />">
<!-- Site CSS -->
<link rel="stylesheet" href="<c:url value='/template/web/style.css' />">
<!-- Responsive CSS -->
<link rel="stylesheet"
	href="<c:url value='/template/web/css/responsive.css' />">
<!-- Custom CSS -->
<link rel="stylesheet"
	href="<c:url value='/template/web/css/custom.css' />">
<script src="<c:url value='/template/web/js/modernizr.js' />"></script>
<script src="https://kit.fontawesome.com/e9cbf7d41b.js" crossorigin="anonymous"></script>
<!-- Modernizr -->

<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" class="politics_version">

	<!-- header -->
	<%@ include file="/common/web/header.jsp"%>
	<!-- header -->

	<!-- body -->
	<dec:body/>
	<!-- /body -->

	<!-- footer -->
	<%@ include file="/common/web/footer.jsp"%>
	<!-- footer -->

	<!-- ALL JS FILES -->
	<script src="<c:url value='/template/web/js/all.js' />"></script>
	<!-- Camera Slider -->
	<script
		src="<c:url value='/template/web/js/jquery.mobile.customized.min.js' />"></script>
	<script src="<c:url value='/template/web/js/jquery.easing.1.3.js' />"></script>
	<script src="<c:url value='/template/web/js/parallaxie.js' />"></script>
	<script src="<c:url value='/template/web/js/headline.js' />"></script>
	<!-- Contact form JavaScript -->
	<script
		src="<c:url value='/template/web/js/jqBootstrapValidation.js' />"></script>
	<script src="<c:url value='/template/web/js/contact_me.js' />"></script>
	<!-- ALL PLUGINS -->
	<script src="<c:url value='/template/web/js/custom.js' />"></script>
	<script src="<c:url value='/template/web/js/jquery.vide.js' />"></script>
</body>
</html>