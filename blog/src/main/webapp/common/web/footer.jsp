<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<div class="copyrights">
	<div class="container">
		<div class="footer-distributed">
			<div class="footer-left">
				<p class="footer-links">
					<c:forEach var="item" items="${categorys}">
						<a href="#">${item.name}</a>
					</c:forEach>
				</p>
				<p class="footer-company-name">
					All Rights Reserved. &copy; 2021 <a href="#">NB OAI</a> Design By :
					<a href="https://html.design/">html design</a>
				</p>
			</div>
		</div>
	</div>
	<!-- end container -->
</div>
<!-- end copyrights -->
<a href="#" id="scroll-to-top" class="dmtop global-radius"><i
	class="fa fa-angle-up"></i></a>
</html>