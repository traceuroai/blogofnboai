<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>

<div id="" class="section lb">
	<div class="container">
		<div class="section-title text-left">
			<h3>
				<c:forEach var="item" items="${categorys}">
					<c:if test="${item.code == 'it-cuocsong'}">
						${item.name}
					</c:if>
				</c:forEach>
			</h3>
			<p>Nơi chia sẻ những điều thú vị xoay quanh ngành it</p>
		</div>
		<!-- end title -->

		<div class="row">
			<c:forEach var="item" items="${news}">
				<c:if test="${item.categoryCode == 'it-cuocsong'}">
					<div class="col-md-4 col-sm-6 col-lg-4">
						<div class="post-box">
							<div class="post-thumb">
								<img src="<c:url value='/template/web/uploads/blog-01.jpg' />"
									class="img-fluid" alt="post-img" />
								<div class="date" style="font-size: 10px;">
									<span>${item.createdDate}</span>
								</div>
							</div>
							<div class="post-info">
								<h4>
									<a href="<c:url value='/chi-tiet/${item.id}' />" title="Xem chi tiết">${item.title}</a>
								</h4>
								<ul>
									<li>by admin</li>
									<%-- <li>${item.createdDate}</li> --%>
									<!-- <li><a href="#"><b> Comments</b></a></li> -->
									<li><i class="fas fa-eye"
										style="margin-right: 7px; margin-left: 10px;">
											${item.views} </i></li>
									<li><a href="#"><i class="far fa-heart"></i></a>
										${item.likes}</li>
								</ul>
								<p>${item.shortDescription}</p>
							</div>
						</div>
					</div>
				</c:if>
			</c:forEach>
		</div>

	</div>
</div>