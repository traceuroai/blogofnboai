<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">

				<div class="login100-form-title"
					style="background-image: url(<c:url value = '/template/login/images/bg-01.jpg' />);">
					<span class="login100-form-title-1"> Sign in </span>
				</div>

				<c:if test="${param.incorrectAccount != null}">
					<div class="alert alert-danger">UserName or Password incorrect !!!</div>
				</c:if>
				
				<c:if test="${param.accessDenied != null}">
					<div class="alert alert-danger">You not authorize !!!</div>
				</c:if>

				<form class="login100-form validate-form"
					action="j_spring_security_check" method="POST">
					<div class="wrap-input100 validate-input m-b-26"
						data-validate="Username is required">
						<span class="label-input100">Email</span> <input class="input100"
							type="email" name="j_username" placeholder="Enter Email"> <span
							class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18"
						data-validate="Password is required">
						<span class="label-input100">Password</span> <input
							class="input100" type="password" name="j_password"
							placeholder="Enter password"> <span
							class="focus-input100"></span>
					</div>

					<div class="flex-sb-m w-full p-b-30">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox"
								name="remember-me"> <label class="label-checkbox100"
								for="ckb1"> Remember me </label>
						</div>
						<div>
							<a href="#" class="txt1"> Forgot Password? </a>
						</div>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">Login</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>