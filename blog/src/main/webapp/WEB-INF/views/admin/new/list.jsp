<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<c:url var="newAPI" value="/admin67/new-api" />
<c:url var="newURL" value="/admin67/new/list?page=1&limit=3" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách bài viết</title>
</head>

<body>
		<div class="main-content">
		<form action="<c:url value = '/admin67/new/list' />" id="formSubmit"
			method="get">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li><i class="ace-icon fa fa-home home-icon"></i> <a href="<c:url value="/admin67/home"/>">Trang
								chủ</a></li>
					</ul>
					<!-- /.breadcrumb -->
				</div>
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<c:if test="${not empty message}">
								<div class="alert alert-${alert}">${message}</div>
							</c:if>
 							<div class="widget-box table-filter">
									<div class="table-btn-controls">
										<div class="pull-right tableTools-container">
											<div class="dt-buttons btn-overlap btn-group">
												<a flag="info"
												   class="dt-button buttons-colvis btn btn-white btn-primary btn-bold" data-toggle="tooltip"
												   title='Thêm bài viết' href='<c:url value="/admin67/new/edit"/>'>
															<span>
																<i class="fa fa-plus-circle bigger-110 purple"></i>
															</span>
												</a>
												<button id="btnDelete" type="button" onclick="warningBeforeDelete()"
														class="dt-button buttons-html5 btn btn-white btn-primary btn-bold" data-toggle="tooltip" title='Xóa bài viết'>
																<span>
																	<i class="fa fa-trash-o bigger-110 pink"></i>
																</span>
												</button>
											</div>
										</div>
									</div>
								</div> 
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th><input type="checkbox" id="checkAll"></th>
													<th>Id</th>
													<th>Tiêu đề</th>
													<th>Hình ảnh</th>
													<th>Mô tả ngắn</th>
													<th>Nội dung</th>
													<th>Thể loại</th>
													<th>views</th>
													<th>likes</th>
													<th>Ngày tạo</th>
													<th>Người tạo</th>
													<th>Ngày thay đổi</th>
													<th>Người thay đổi</th>
													<th>Thao tác</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="item" items="${model.listResult}">
													<tr>
														<td><input type="checkbox" id="checkbox_${item.id}" value="${item.id}"></td>
														<td>${item.id}</td>
														<td>${item.title}</td>
														<td>${item.thumbnail}</td>
														<td>${item.shortDescription}</td>
														<td>${item.content}</td>
														<td>${item.categoryId}</td>
														<td>${item.views}</td>
														<td>${item.likes}</td>
														<td>${item.createdDate}</td>
														<td>${item.createdBy}</td>
														<td>${item.modifiedDate}</td>
														<td>${item.modifiedBy}</td>
														<td>
															<c:url var="editURL" value="/admin67/new/edit">
																<c:param name="id" value="${item.id}"/>
															</c:url>
															<a class="btn btn-sm btn-primary btn-edit" data-toggle="tooltip"
															   title="Cập nhật bài viết" href='${editURL}'><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
															</a>
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<ul class="pagination" id="pagination"></ul>
										<input type="hidden" value="" id="page" name="page" />
										<input type="hidden" value="" id="limit" name="limit" />
											<!-- <input type="hidden" value="" id="sortName" name="sortName" />
											<input type="hidden" value="" id="sortBy" name="sortBy" /> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<script>
		// khi server gửi lên view -> sẽ lấy được page, tổng số page, số item / 1 page.
 		var currentPage = ${model.page};
		var totalPages = ${model.totalPage};
		var limit = 3;// số bài viết trong 1 page.
		$(function() {
			window.pagObj = $('#pagination').twbsPagination({
				totalPages : totalPages,// tổng số page
				visiblePages : 10,// số item trong thanh Page.
				startPage : currentPage,// số trang bắt đầu đứng khi hiển thị trang
				onPageClick : function(event, page) {// giá trị tham số page này sẽ thay đổi khi ta click qua các item
					//console.info(page + ' (from options)');
					// tránh trường hợp gọi load lại page liên tục -> check nếu đang đứng thì ko load nữa.
					if (currentPage != page) {
						$('#page').val(page)
						$('#limit').val(limit)
						$('#formSubmit').submit();// gọi submit -> gửi lên server bằng form
					}
				}
			})/* .on('page', function(event, page) {
				console.info(page + ' (from event listening)');
			}); */
		});
		
		function warningBeforeDelete() {
			swal({
				  title: "Xác nhận xóa",
				  text: "Bạn có chắc muốn xóa ?",
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonClass: "btn-success",
				  cancelButtonClass: "btn-danger",
				  confirmButtonText: "Yes, delete it!",
				  cancelButtonText: "No, cancel plx!",
				}).then(function(isConfirm) {
				  if (isConfirm) {
					var ids = $('tbody input[type=checkbox]:checked').map(function () {// lấy được hết các giá trị đã check(lấy từ tbody)
			            return $(this).val();
			        }).get();
					deleteNew(ids);
				  } else {
				    swal("Cancelled", "Your imaginary file is safe :)", "error");
				  }
				});
		}
		
		function deleteNew(data)
		{
			$.ajax({
				url: '${newAPI}',// không được gọi trực tiếp (phải qua c:url) -> khai báo trên đầu
				type: 'DELETE',
				contentType: 'application/json',
				data: JSON.stringify(data),// dữ liệu gửi lên server (đã chuyển từ jsObject -> json)
				//dataType: 'json',
				success: function(result){
					window.location.href = "${newURL}&message=delete_success";
				},
				error: function(error){
					window.location.href = "${newURL}&message=error_system";
				}
			});
		}
	</script>

</body>
</html>
