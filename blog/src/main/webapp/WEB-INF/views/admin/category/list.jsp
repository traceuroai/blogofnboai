<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<c:url var="categoryAPI" value="/admin67/category-api" />
<c:url var="categoryURL" value="/admin67/category/list" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách thể loại</title>
</head>

<body>
	<div class="main-content">
		<form id="formSubmit" method="get">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Trang
								chủ</a></li>
					</ul>
					<!-- /.breadcrumb -->
				</div>
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<c:if test="${not empty message}">
								<div class="alert alert-${alert}">${message}</div>
							</c:if>
							<div class="widget-box table-filter">
								<div class="table-btn-controls">
									<div class="pull-right tableTools-container">
										<div class="dt-buttons btn-overlap btn-group">
											<a flag="info"
												class="dt-button buttons-colvis btn btn-white btn-primary btn-bold"
												data-toggle="tooltip" title='Thêm thể loại'
												href='<c:url value="/admin67/category/edit"/>'> <span>
													<i class="fa fa-plus-circle bigger-110 purple"></i>
											</span>
											</a>
											<button id="btnDelete" type="button"
												onclick="warningBeforeDelete()"
												class="dt-button buttons-html5 btn btn-white btn-primary btn-bold"
												data-toggle="tooltip" title='Xóa thể loại'>
												<span> <i class="fa fa-trash-o bigger-110 pink"></i>
												</span>
											</button>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th><input type="checkbox" id="checkAll"></th>
													<th>Id</th>
													<th>Code</th>
													<th>Name</th>
													<th>Ngày tạo</th>
													<th>Người tạo</th>
													<th>Ngày thay đổi</th>
													<th>Người thay đổi</th>
													<th>Thao tác</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="item" items="${categorys.listResult}">
													<tr>
														<td><input type="checkbox" id="checkbox_${item.id}"
															value="${item.id}"></td>
														<td>${item.id}</td>
														<td>${item.code}</td>
														<td>${item.name}</td>
														<td>${item.createdDate}</td>
														<td>${item.createdBy}</td>
														<td>${item.modifiedDate}</td>
														<td>${item.modifiedBy}</td>
														<td><c:url var="editURL"
																value="/admin67/category/edit">
																<c:param name="id" value="${item.id}" />
															</c:url> <a class="btn btn-sm btn-primary btn-edit"
															data-toggle="tooltip" title="Cập nhật thể loại"
															href='${editURL}'><i class="fa fa-pencil-square-o"
																aria-hidden="true"></i> </a></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<script>
		function warningBeforeDelete() {
			swal(
					{
						title : "Xác nhận xóa",
						text : "Bạn có chắc muốn xóa (xóa thể loại kèm các bài viết thuộc thể loại)?",
						type : "warning",
						showCancelButton : true,
						confirmButtonClass : "btn-success",
						cancelButtonClass : "btn-danger",
						confirmButtonText : "Yes, delete it!",
						cancelButtonText : "No, cancel plx!",
					}).then(
					function(isConfirm) {
						if (isConfirm) {
							var ids = $('tbody input[type=checkbox]:checked')
									.map(function() {
										return $(this).val();
									}).get();
							deleteCategory(ids);
						} else {
							swal("Cancelled", "Your imaginary file is safe :)",
									"error");
						}
					});
		}

		function deleteCategory(data) {
			$
					.ajax({
						url : '${categoryAPI}',
						type : 'DELETE',
						contentType : 'application/json',
						data : JSON.stringify(data),
						success : function(result) {
							window.location.href = "${categoryURL}?message=delete_success";
						},
						error : function(error) {
							window.location.href = "${categoryURL}?message=error_system";
						}
					});
		}
	</script>

</body>
</html>
