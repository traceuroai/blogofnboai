package com.blog.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/*
 * FILE CẤU HÌNH SPRING JPA :
 * @Configuration : là file cấu hình.
 * @EnableTransactionManagement : (transaction manager) quả lí tự động các transaction(commit, rollback, close, ...)
 * @EnableJpaRepositories : để sử dụng được các phương thức trong JpaRepository
 * datasource : load driver, nhận các thông tin truy cập (open connection)
 * additionalProperties : tạo table từ class entity.
 * transactionManager(EntityManagerFactory entityManagerFactory) : bật tính năng tự động quản lí transaction.
 * */

@Configuration
@EnableJpaRepositories(basePackages = "com.blog.repository")
@EnableTransactionManagement
public class JPAConfig {

	// load Container :
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory()// tương tự getConnection()
	{
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());// thông tin truy cập db
		em.setPersistenceUnitName("persistence-data");// (PERSISTENCE.XML)
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());
		return em;
	}

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/blogItSpringMvc");
		dataSource.setUsername("root");
		dataSource.setPassword("baoai");
		return dataSource;
	}

	@Bean
	JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	Properties additionalProperties() {
		Properties properties = new Properties();
//		 chưa có data(khi bắt đầu project) : khi database ổn định -> đóng create-drop lại sẽ không bị tạo mới thực thể nữa.
//		 trường hợp chỉ tạo mới mà không muốn xóa table cũ -> chỉ là create.
//		 properties.setProperty("hibernate.hbm2ddl.auto", "create");
//		 properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
		properties.setProperty("hibernate.hbm2ddl.auto", "none");
		properties.setProperty("hibernate.enable_lazy_load_no_trans", "true");
		return properties;
	}

}
