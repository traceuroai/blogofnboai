package com.blog.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/*
 * @Configuration : khai báo là file config.
 * @EnableJpaAuditing : sử dụng auditing.
 * */

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider") // bật tính năng auditing
public class JPAAuditingConfig {

	@Bean
	public AuditorAware<String> auditorProvider() {
		return new AuditorAwareImpl();
	}

	public static class AuditorAwareImpl implements AuditorAware<String> {
		@Override
		public String getCurrentAuditor() {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();// lấy name.
			if (authentication == null)// không đăng nhập, ....
			{
				return null;
			}
			return authentication.getName();// lấy userName người dùng rồi tìm đến @CreateBy, Modifyby và lưu xuống
											// csdl.
		}
	}

}
