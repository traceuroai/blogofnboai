package com.blog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "new")
@Data
public class NewEntity extends BaseEntity{

	@Column(name = "title")
	private String title;
	@Column(name = "thumbnail")
	private String thumbnail;
	@Column(name = "shotdescription", columnDefinition = "TEXT")
	private String shortDescription;
	@Column(name = "content", columnDefinition = "TEXT")
	private String content;
//	@Column(name = "categoryid")
//	private Long categoryId;
//	@Column(name = "categorycode")
//	private String categoryCode;
	@Column(name = "likes")
	private int likes;
	@Column(name = "views")
	private int views;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "category_id")
	private CategoryEntity category;
	

}
