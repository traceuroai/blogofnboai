//package com.blog.mapper.impl;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import com.blog.mapper.RowMapper;
//import com.blog.model.NewModel;
//
//public class NewMapper implements RowMapper<NewModel> {
//
//	@Override
//	public NewModel Maprow(ResultSet resultSet) {
//		try {
//			NewModel newModel = new NewModel();
//			newModel.setId(resultSet.getLong("id"));
//			newModel.setTitle(resultSet.getString("title"));
//			newModel.setThumbnail(resultSet.getString("thumbnail"));
//			newModel.setShortDescription(resultSet.getString("shortdescription"));
//			newModel.setContent(resultSet.getString("content"));
//			newModel.setCategoryId(resultSet.getLong("categoryid"));
//			newModel.setLike(resultSet.getInt("likes"));
//			newModel.setViews(resultSet.getInt("views"));
//			newModel.setCreatedDate(resultSet.getTimestamp("createddate"));
//			newModel.setCreatedBy(resultSet.getString("createdby"));
////			newModel.setModifiedDate(resultSet.getTimestamp("modifieddate"));
////			newModel.setModifiedBy(resultSet.getString("modifiedby"));
//			if (resultSet.getTimestamp("modifieddate") != null) {
//				newModel.setModifiedDate(resultSet.getTimestamp("modifieddate"));
//			}
//			if (resultSet.getString("modifiedby") != null) {
//				newModel.setModifiedBy(resultSet.getString("modifiedby"));
//			}
//			return newModel;
//		} catch (SQLException e) {
//			return null;
//		}
//	}
//}
