//package com.blog.mapper.impl;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import com.blog.mapper.RowMapper;
//import com.blog.model.CommentModel;
//
//public class CommentMapper implements RowMapper<CommentModel>{
//
//	@Override
//	public CommentModel Maprow(ResultSet resultSet) {
//		try {
//			CommentModel comment = new CommentModel();
//			comment.setId(resultSet.getLong("id"));
//			comment.setContent(resultSet.getString("content"));
//			comment.setNewId(resultSet.getLong("newid"));
//			comment.setUserId(resultSet.getLong("userid"));
//			return comment;
//		} catch (SQLException e) {
//			return null;
//		}
//	}
//
//}
