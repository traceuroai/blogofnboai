//package com.blog.mapper.impl;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import com.blog.mapper.RowMapper;
//import com.blog.model.RoleModel;
//
//public class RoleMapper implements RowMapper<RoleModel>{
//	
//	@Override
//	public RoleModel Maprow(ResultSet resultSet) {
//		try {
//			RoleModel roleModel = new RoleModel();
//			roleModel.setId(resultSet.getLong("id"));
//			roleModel.setCode(resultSet.getString("code"));
//			roleModel.setName(resultSet.getString("name"));
//			roleModel.setCreatedDate(resultSet.getTimestamp("createddate"));
//			roleModel.setCreatedBy(resultSet.getString("createdby"));
//			roleModel.setModifiedDate(resultSet.getTimestamp("modifieddate"));
//			roleModel.setModifiedBy(resultSet.getString("modifiedby"));
//			return roleModel;
//		} catch (SQLException e) {
//			return null;
//		}
//	}
//
//}
