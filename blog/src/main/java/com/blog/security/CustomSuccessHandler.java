package com.blog.security;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.blog.util.securityUtils;

/*
 * CustomSuccessHandler : sẽ thực hiện việc phân quyền.
 * kế thừa SimpleUrlAuthenticationSuccessHandler do đó sẽ override lại phương thức sẽ phân chia quyền hạn.
 * phương thức determineTargetUrl sẽ trả về đúng giao diện người dùng bằng cách kiểm tra xem quyền người dùng
 * là user hay admin bằng cách kiểm tra role mà sau khi authentication đã set.
 * */

@Component// free style (DI)
public class CustomSuccessHandler extends SimpleUrlAuthenticationSuccessHandler{
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException {
		String targetUrl = determineTargetUrl(authentication);
		if (response.isCommitted()) {
			return;
		}
		redirectStrategy.sendRedirect(request, response, targetUrl);// trả về url.
	}
	
	public RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}

	private String determineTargetUrl(Authentication authentication) {
		String url = "";
		// để biết user hay admin -> phải get được role của nó (đã put thông tin vào session chưa ?)
		List<String> roles = securityUtils.getAuthorities();// bên securityUtil đã lấy được all code của role.
		if(isAdmin(roles))
		{
			url = "/admin67/home";
		}else if(isUser(roles))
		{
			url = "/trang-chu";
		}
		return url;
	}
	
	private boolean isAdmin(List<String> roles)
	{
		if(roles.contains("ADMIN"))
		{
			return true;
		}
		return false;
	}
	
	private boolean isUser(List<String> roles)
	{
		if(roles.contains("USER"))
		{
			return true;
		}
		return false;
	}

}
