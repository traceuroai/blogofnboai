package com.blog.converter;

import org.springframework.stereotype.Component;

import com.blog.dto.NewDTO;
import com.blog.entity.NewEntity;

@Component
public class NewConverter {

	public NewDTO toDTO(NewEntity entity)
	{
		NewDTO result = new NewDTO();
		result.setId(entity.getId());
		result.setTitle(entity.getTitle());
		result.setThumbnail(entity.getThumbnail());
		result.setShortDescription(entity.getShortDescription());
		result.setContent(entity.getContent());
		// do sử dụng ràng buộc vs cơ chế là lazy -> sẽ có category lên theo new
		result.setCategoryId(entity.getCategory().getId());
		result.setCategoryCode(entity.getCategory().getCode());
		result.setLikes(entity.getLikes());
		result.setViews(entity.getViews());
		result.setCreatedDate(entity.getCreatedDate());
		result.setCreatedBy(entity.getCreatedBy());
		if(entity.getModifiedDate() != null)
		{
			result.setModifiedDate(entity.getModifiedDate());
		}
		if(entity.getModifiedBy() != null)
		{
			result.setModifiedBy(entity.getModifiedBy());
		}
		return result;
	}
	
	public NewEntity toEntity(NewDTO newDto)
	{
		NewEntity result = new NewEntity();
		// id là field tự tăng -> không có chuyện set
		result.setTitle(newDto.getTitle());
		result.setThumbnail(newDto.getThumbnail());
		result.setShortDescription(newDto.getShortDescription());
		result.setContent(newDto.getContent());
//		result.setCategory();
		result.setLikes(newDto.getLikes());
		result.setViews(newDto.getViews());
		result.setCreatedDate(newDto.getCreatedDate());
		result.setCreatedBy(newDto.getCreatedBy());
		if(newDto.getModifiedDate() != null)
		{
			result.setModifiedDate(newDto.getModifiedDate());
		}
		if(newDto.getModifiedBy() != null)
		{
			result.setModifiedBy(newDto.getModifiedBy());
		}
		return result;
	}
	
	public NewEntity toEntity(NewDTO newDto, NewEntity oldNew)
	{
		oldNew.setTitle(newDto.getTitle());
		oldNew.setThumbnail(newDto.getThumbnail());
		oldNew.setShortDescription(newDto.getShortDescription());
		oldNew.setContent(newDto.getContent());
		oldNew.setLikes(newDto.getLikes());
		oldNew.setViews(newDto.getViews());
		return oldNew;
	}
	
}
