package com.blog.converter;

import org.springframework.stereotype.Component;

import com.blog.dto.CategoryDTO;
import com.blog.entity.CategoryEntity;

@Component
public class CategoryConverter {
	
	public CategoryDTO toDTO(CategoryEntity entity)
	{
		CategoryDTO result = new CategoryDTO();
		result.setId(entity.getId());
		result.setCode(entity.getCode());
		result.setName(entity.getName());
		result.setCreatedDate(entity.getCreatedDate());
		result.setCreatedBy(entity.getCreatedBy());
		if(entity.getModifiedDate() != null)
		{
			result.setModifiedDate(entity.getModifiedDate());
		}
		if(entity.getModifiedBy() != null)
		{
			result.setModifiedBy(entity.getModifiedBy());
		}
		return result;
	}
	
	public CategoryEntity toEntity(CategoryDTO categoryDTO)
	{
		CategoryEntity result = new CategoryEntity();
		result.setCode(categoryDTO.getCode());
		result.setName(categoryDTO.getName());
		result.setCreatedDate(categoryDTO.getCreatedDate());
		result.setCreatedBy(categoryDTO.getCreatedBy());
		if(categoryDTO.getModifiedDate() != null)
		{
			result.setModifiedDate(categoryDTO.getModifiedDate());
		}
		if(categoryDTO.getModifiedBy() != null)
		{
			result.setModifiedBy(categoryDTO.getModifiedBy());
		}
		return result;
	}
	
	public CategoryEntity toEntity(CategoryDTO categoryDto, CategoryEntity oldCategory)
	{
		oldCategory.setCode(categoryDto.getCode());
		oldCategory.setName(categoryDto.getName());
		return oldCategory;
	}

}
