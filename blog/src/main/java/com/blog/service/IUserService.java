package com.blog.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.blog.dto.UserDTO;

public interface IUserService {

	List<UserDTO> findAll(Pageable pageable);

	int getTotalItem();

}
