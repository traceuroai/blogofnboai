package com.blog.service;

import java.util.List;
import java.util.Map;

import com.blog.dto.CategoryDTO;

public interface ICategoryService {
	
	List<CategoryDTO> findAll();
	
	CategoryDTO findOneById(long Id);
	
	Map<String, String> findAllTypeMap();
	
	CategoryDTO save(CategoryDTO categoryDto);
	
	CategoryDTO update(CategoryDTO categoryDto);
	
	void delete(long[] ids);

}
