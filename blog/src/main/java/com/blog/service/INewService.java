package com.blog.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.blog.dto.NewDTO;

public interface INewService {
	
	List<NewDTO> findAll(Pageable pageable);
	
	List<NewDTO> findALL();
	
	int getTotalItem();

	NewDTO findOneById(long id);
	
//	NewDTO insert(NewDTO newdto);
//	
//	NewDTO update(NewDTO updateNew);
	
	NewDTO save(NewDTO newdto);
	
	void deleteNew(long[] ids);
	
	List<NewDTO> findByCategoryCode(String code);
	
	NewDTO updateLike(long id);
}
