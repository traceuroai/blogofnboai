package com.blog.service;

import java.util.List;

import com.blog.entity.CategoryEntity;

public interface MenuService {
	
	List<CategoryEntity> listMenu();

}
