package com.blog.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blog.entity.CategoryEntity;
import com.blog.repository.CategoryRepository;
import com.blog.service.MenuService;

@Service
public class MenuServiceImpl implements MenuService{
	
	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public List<CategoryEntity> listMenu() {
		return categoryRepository.findAll();
	}

	
}
