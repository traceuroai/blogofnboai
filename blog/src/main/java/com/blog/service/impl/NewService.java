package com.blog.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.blog.converter.NewConverter;
import com.blog.dto.NewDTO;
import com.blog.entity.CategoryEntity;
import com.blog.entity.NewEntity;
import com.blog.repository.CategoryRepository;
import com.blog.repository.NewRepository;
import com.blog.service.INewService;

@Service
public class NewService implements INewService {

	@Autowired
	private NewRepository newRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private NewConverter newConverter;

	@Override
	public List<NewDTO> findAll(Pageable pageable) {
		List<NewEntity> entities = newRepository.findAll(pageable).getContent();
		List<NewDTO> news = new ArrayList<>();
		for (NewEntity item : entities) {
			NewDTO newdto = newConverter.toDTO(item);
			news.add(newdto);
		}
		return news;
	}

	@Override
	public int getTotalItem() {
		return (int) newRepository.count();
	}

	@Override
	public NewDTO findOneById(long id) {
		NewEntity news = newRepository.findOne(id);
		news.setViews(news.getViews() + 1);
		newRepository.save(news);
		return newConverter.toDTO(news);
	}

//	@Override
//	@Transactional
//	public NewDTO insert(NewDTO newdto) {
//		CategoryEntity category = categoryRepository.findOneByCode(newdto.getCategoryCode());
//		NewEntity newEntity = newConverter.toEntity(newdto);
//		newEntity.setCategory(category);
//		return newConverter.toDTO(newRepository.save(newEntity));
//	}
//
//	@Override
//	@Transactional
//	public NewDTO update(NewDTO newDto) {
//		NewEntity oldNew = newRepository.findOne(newDto.getId());
//		CategoryEntity category = categoryRepository.findOneByCode(newDto.getCategoryCode());
//		oldNew.setCategory(category);
//		NewEntity updateNew = newConverter.toEntity(newDto, oldNew);
//		return newConverter.toDTO(newRepository.save(updateNew));
//	}

	@Override
	@Transactional
	public NewDTO save(NewDTO newDto) {
		CategoryEntity category = categoryRepository.findOneByCode(newDto.getCategoryCode());
		NewEntity newEntity = new NewEntity();
		if(newDto.getId() != null)
		{
			NewEntity oldNew = newRepository.findOne(newDto.getId());
			oldNew.setCategory(category);
			newEntity = newConverter.toEntity(newDto, oldNew);
		}
		else
		{
			newEntity = newConverter.toEntity(newDto);
			newEntity.setCategory(category);
		}
		return newConverter.toDTO(newRepository.save(newEntity));
	}

	@Override
	@Transactional
	public void deleteNew(long[] ids) {
		for (long id : ids) {
			newRepository.delete(id);
		}
	}

	@Override
	public List<NewDTO> findALL() {
		List<NewEntity> entities = newRepository.findAll();
		List<NewDTO> result = new ArrayList<>();
		for (NewEntity news : entities) {
			NewDTO item = newConverter.toDTO(news);
			result.add(item);
		}
		return result;
	}

	@Override
	public List<NewDTO> findByCategoryCode(String code) {
		List<NewEntity> entities = newRepository.findByCategoryCode(code);
		List<NewDTO> result = new ArrayList<>();
		for (NewEntity news : entities) {
			NewDTO item = newConverter.toDTO(news);
			result.add(item);
		}
		return result;
	}

	@Override
	public NewDTO updateLike(long id) {
		NewEntity news = newRepository.findOneById(id);
		news.setLikes(news.getLikes());
		newRepository.save(news);
		return null;
	}
	
}
