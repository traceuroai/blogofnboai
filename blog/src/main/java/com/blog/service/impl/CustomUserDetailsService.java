package com.blog.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.blog.constant.systemConstant;
import com.blog.dto.MyUser;
import com.blog.entity.RoleEntity;
import com.blog.entity.UserEntity;
import com.blog.repository.UserRepository;

/*
 * CustomUserDetailsService : thực hiện việc authentication.
 * phương thức loadUserByUsername sẽ tìm thông tin của người dùng theo username, password, status.
 * phần password đã được ngầm sử lí (MD5) bên security -> chỉ cần nhận userName
 * do là @Service nên sẽ gọi đến @Repository (mô hình 3-tier) để thực hiện kiểm tra người dùng tồn tại.
 * nếu không tồn tại tài khoản người dùng -> throw 1 exception trả về thông báo lỗi.
 * nếu người dùng tồn tại : sẽ set 1 sessoin chứa thông tin của người dùng.
 */

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// phần password đã ngầm được xử lý
		UserEntity userEntity = userRepository.findOneByUserNameAndStatus(username, systemConstant.ACTIVE_STATUS);

		if (userEntity == null) {
			throw new UsernameNotFoundException("user not found");// trả về failed trong security.xml
		}
		
		// put thông tin sau khi user login vào hệ thống (sp security tạo sẵn user)
		// authoriries : là role
		
		// authorities : role (getCode)
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (RoleEntity role : userEntity.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getCode()));
		}
		// nếu user tồn tại -> put thông tin vào security (để duy trì thông tin) khi user login vào hệ thống.
		// MyUser custom lại user của spring.
		MyUser user = new MyUser(userEntity.getUserName(), userEntity.getPassword(), true, true, true, true,
				authorities);
		user.setFullname(userEntity.getFullName());

		return user;// MyUser extends từ User mà User implement UserDetail -> MyUser là 1 UserDetail
	}

}
