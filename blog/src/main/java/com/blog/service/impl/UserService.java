package com.blog.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.blog.dto.UserDTO;
import com.blog.entity.UserEntity;
import com.blog.repository.UserRepository;
import com.blog.service.IUserService;

@Service
public class UserService implements IUserService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public List<UserDTO> findAll(Pageable pageable) {
		List<UserEntity> entities = userRepository.findAll(pageable).getContent();
		List<UserDTO> users = new ArrayList<>();
		for (UserEntity item : entities) {
			UserDTO userdto = new UserDTO();
			userdto.setId(item.getId());
			userdto.setUserName(item.getUserName());
			userdto.setFullName(item.getFullName());
			userdto.setPassword(item.getPassword());
			userdto.setPhone(item.getPhone());
			userdto.setStatus(item.getStatus());
			userdto.setCreatedDate(item.getCreatedDate());
			userdto.setCreatedBy(item.getCreatedBy());
			if(item.getModifiedDate() != null)
			{
				userdto.setModifiedDate(item.getModifiedDate());
			}
			if(item.getModifiedBy() != null)
			{
				userdto.setModifiedBy(item.getModifiedBy());
			}
			users.add(userdto);
		}
		return users;
	}

	@Override
	public int getTotalItem() {
		return (int) userRepository.count();
	}

}
