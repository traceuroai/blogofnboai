package com.blog.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.blog.converter.CategoryConverter;
import com.blog.dto.CategoryDTO;
import com.blog.entity.CategoryEntity;
import com.blog.entity.NewEntity;
import com.blog.repository.CategoryRepository;
import com.blog.repository.NewRepository;
import com.blog.service.ICategoryService;

@Service
public class CategoryService implements ICategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private NewRepository newRepository;

	@Autowired
	private CategoryConverter categoryConverter;

	@Override
	public List<CategoryDTO> findAll() {
		List<CategoryDTO> categorys = new ArrayList<>();
		List<CategoryEntity> entities = categoryRepository.findAll();
		for (CategoryEntity item : entities) {
			CategoryDTO categorydto = categoryConverter.toDTO(item);
			categorys.add(categorydto);
		}
		return categorys;
	}

	@Override
	public CategoryDTO findOneById(long Id) {
		return categoryConverter.toDTO(categoryRepository.findOne(Id));
	}

	@Override
	public Map<String, String> findAllTypeMap() {
		Map<String, String> result = new HashMap<>();
		List<CategoryEntity> entities = categoryRepository.findAll();
		for (CategoryEntity categoryEntity : entities) {
			result.put(categoryEntity.getCode(), categoryEntity.getName());
		}
		return result;
	}

	@Override
	@Transactional
	public CategoryDTO save(CategoryDTO categoryDto) {
		return categoryConverter.toDTO(categoryRepository.save(categoryConverter.toEntity(categoryDto)));
	}

	@Override
	@Transactional
	public CategoryDTO update(CategoryDTO categoryDto) {
		CategoryEntity oldCategory = categoryRepository.findOne(categoryDto.getId());
		CategoryEntity updateCategory = categoryConverter.toEntity(categoryDto, oldCategory);
		return categoryConverter.toDTO(categoryRepository.save(updateCategory));
	}

	@Override
	@Transactional
	public void delete(long[] ids) {
		for (long id : ids) {
			CategoryEntity tl = categoryRepository.findOne(id);
			List<NewEntity> newEntity = newRepository.findByCategoryCode(tl.getCode());
			for (NewEntity item : newEntity) {
				newRepository.delete(item.getId());
			}
			categoryRepository.delete(id);
		}
	}

}
