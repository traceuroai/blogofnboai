package com.blog.controller.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.blog.dto.CategoryDTO;
import com.blog.service.ICategoryService;
import com.blog.util.messageUtils;

@Controller()
public class CategoryController {
	
	@Autowired
	private ICategoryService categoryService;
	
	@Autowired
	private messageUtils messageUtil;
	
	@RequestMapping(value = "/admin67/category/list", method = RequestMethod.GET)
	public ModelAndView showListNews(HttpServletRequest req) {
		CategoryDTO categorys = new CategoryDTO();
		ModelAndView mav = new ModelAndView("admin/category/list");
		categorys.setListResult(categoryService.findAll());
		if (req.getParameter("message") != null) {
			Map<String, String> message = messageUtil.getMessage(req.getParameter("message"));
			mav.addObject("message", message.get("message"));
			mav.addObject("alert", message.get("alert"));
		}
		mav.addObject("categorys", categorys);
		return mav;
	}

	@RequestMapping(value = "/admin67/category/edit", method = RequestMethod.GET)
	public ModelAndView editNews(@RequestParam(value = "id", required = false) Long id, HttpServletRequest req) {
		ModelAndView mav = new ModelAndView("admin/category/edit");
		CategoryDTO model = new CategoryDTO();
		if (id != null) {
			model = categoryService.findOneById(id);
		}
		if (req.getParameter("message") != null) {
			Map<String, String> message = messageUtil.getMessage(req.getParameter("message"));
			mav.addObject("message", message.get("message"));
			mav.addObject("alert", message.get("alert"));
		}
		mav.addObject("model", model);
		return mav;
	}

}
