package com.blog.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.blog.dto.UserDTO;
import com.blog.service.IUserService;

@Controller
public class UserController {

	@Autowired
	private IUserService userService;
	
	@RequestMapping(value = "/admin67/user/list", method = RequestMethod.GET)
	public ModelAndView showListNews(@RequestParam("page") int page, @RequestParam("limit") int limit) {
		UserDTO users = new UserDTO();
		users.setPage(page);
		users.setLimit(limit);
		ModelAndView mav = new ModelAndView("admin/user/list");
		Pageable pageable = new PageRequest(page - 1, limit);
		users.setListResult(userService.findAll(pageable));
		users.setTotalItem(userService.getTotalItem());
		users.setTotalPage( (int) Math.ceil((double) users.getTotalItem() / users.getLimit()));
		mav.addObject("model", users);
		return mav;
	}

	@RequestMapping(value = "/admin67/user/edit", method = RequestMethod.GET)
	public ModelAndView editNews() {
		ModelAndView mav = new ModelAndView("admin/user/edit");
		return mav;
	}
	
}
