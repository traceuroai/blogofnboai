package com.blog.controller.admin;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.blog.dto.CategoryDTO;
import com.blog.dto.NewDTO;
import com.blog.service.ICategoryService;
import com.blog.service.INewService;
import com.blog.util.messageUtils;

@Controller(value = "newControllerOfAdmin")
public class NewController {

	@Autowired
	private INewService newService;

	@Autowired
	private ICategoryService categoryService;

	@Autowired
	private messageUtils messageUtil;

	@RequestMapping(value = "/admin67/new/list", method = RequestMethod.GET)
	public ModelAndView showListNews(@RequestParam("page") int page, @RequestParam("limit") int limit,
			HttpServletRequest req) {
		NewDTO model = new NewDTO();
		model.setPage(page);
		model.setLimit(limit);
		ModelAndView mav = new ModelAndView("admin/new/list");
		Sort sort = new Sort(Sort.DEFAULT_DIRECTION.DESC, "id");
		Pageable pageable = new PageRequest(page - 1, limit, sort);
		model.setListResult(newService.findAll(pageable));
		model.setTotalItem(newService.getTotalItem());
		model.setTotalPage((int) Math.ceil((double) model.getTotalItem() / model.getLimit()));
		if (req.getParameter("message") != null) {
			Map<String, String> message = messageUtil.getMessage(req.getParameter("message"));
			mav.addObject("message", message.get("message"));
			mav.addObject("alert", message.get("alert"));
		}
		mav.addObject("model", model);
		return mav;
	}

	/*
	 * url sẽ sử dụng cho 2 trường hợp là thêm mới và cập nhật, phân biệt nhau qua
	 * id. -> sử dụng @RequestPram muốn không lỗi trong trường hợp không có id : làm
	 * như sau
	 */
	@RequestMapping(value = "/admin67/new/edit", method = RequestMethod.GET)
	public ModelAndView editNews(@RequestParam(value = "id", required = false) Long id, HttpServletRequest req) {
		ModelAndView mav = new ModelAndView("admin/new/edit");
		NewDTO model = new NewDTO();
		if (id != null) {
			model = newService.findOneById(id);
		}
		if (req.getParameter("message") != null) {
			Map<String, String> message = messageUtil.getMessage(req.getParameter("message"));
			mav.addObject("message", message.get("message"));
			mav.addObject("alert", message.get("alert"));
		}
		mav.addObject("model", model);
		return mav;
	}
	
//	@PostMapping(value = "/admin67/addnew")
//	public NewDTO createNew (@ModelAttribute("model") NewDTO newDTO )
//	{
//		return newService.save(newDTO);
//	}

	@ModelAttribute(name = "categories")
	public Map<String, String> categorys() {
		return categoryService.findAllTypeMap();
	}

}
