package com.blog.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.blog.dto.NewDTO;
import com.blog.service.INewService;

@Controller(value = "homeControllerOfWeb")
public class HomeController {
	
	@Autowired
	private INewService newService;

	@RequestMapping(value = "/trang-chu", method = RequestMethod.GET)
	public ModelAndView homePage() {
		ModelAndView mav = new ModelAndView("web/home");
		mav.addObject("active", "trang-chu");// để client nhận biết trang hoạt động
		mav.addObject("news" , newService.findALL());
		return mav;
	}
	
	@RequestMapping(value = "/it-cuocsong", method = RequestMethod.GET)
	public ModelAndView homePage1() {
		ModelAndView mav = new ModelAndView("web/it-cuocsong");
		mav.addObject("active", "it-cuocsong");
		mav.addObject("news" , newService.findALL());
		return mav;
	}
	
	@RequestMapping(value = "/kien-thuc", method = RequestMethod.GET)
	public ModelAndView homePage2() {
		ModelAndView mav = new ModelAndView("web/kien-thuc");
		mav.addObject("active", "kien-thuc");
		mav.addObject("news" , newService.findALL());
		return mav;
	}
	
	@RequestMapping(value = "/tai-lieu", method = RequestMethod.GET)
	public ModelAndView homePage3() {
		ModelAndView mav = new ModelAndView("web/tai-lieu");
		mav.addObject("active", "tai-lieu");
		mav.addObject("news" , newService.findALL());
		return mav;
	}
	
	@RequestMapping(value = "/ve-toi", method = RequestMethod.GET)
	public ModelAndView homePage4() {
		ModelAndView mav = new ModelAndView("web/ve-toi");
		mav.addObject("active", "ve-toi");
		mav.addObject("news" , newService.findALL());
		return mav;
	}
	
	@RequestMapping(value = "/chi-tiet/{id}", method = RequestMethod.GET)
	public ModelAndView homePage5(@PathVariable(required = true) int id) {
		ModelAndView mav = new ModelAndView("web/chi-tiet");
		NewDTO newDto = newService.findOneById(id);
		mav.addObject("news", newService.findByCategoryCode(newDto.getCategoryCode()));
		mav.addObject("new1" , newDto);
		return mav;
	}

}