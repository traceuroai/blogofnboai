package com.blog.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.blog.dto.MyUser;

public class securityUtils {

	// lấy dl được put vào MyUser ra.
	public static MyUser getPrincipal() {
		MyUser myUser = (MyUser) (SecurityContextHolder.getContext()).getAuthentication().getPrincipal();
		return myUser;
	}

	public static List<String> getAuthorities() {
		List<String> result = new ArrayList<>();
		// get dl từ authorities (bên CustomUserDetailsService đã put)
		List<GrantedAuthority> authorities = (List<GrantedAuthority>) (SecurityContextHolder.getContext()
				.getAuthentication().getAuthorities());
		for (GrantedAuthority au : authorities) {
			result.add(au.getAuthority());
		}
		return result;
	}

}
