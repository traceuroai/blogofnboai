package com.blog.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class uploadFileUtils {


	private final String root = "src/main/resources";

	public String writeOrUpdate(byte[] bytes, String path) {
		String url = null;
		// path1 = path (tên file truyền từ api xuống).
		// path2 = root + path1
		// kiểm tra folder đã tồn tại chưa, tạo folder
		File file = new File(StringUtils.substringBeforeLast(root + path, "/"));
		if (!file.exists()) {
			file.mkdirs();
		}
		// ghi file
		FileOutputStream fileOutputStream = null;
		try {// chú ý (java 7 trở lên được áp dụng)
			fileOutputStream = new FileOutputStream(new File(root + path));
			fileOutputStream.write(bytes);
			url = file.getAbsolutePath();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if(fileOutputStream != null)
				{
					fileOutputStream.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return url;
	}

}
