package com.blog.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class messageUtils {

	public Map<String, String> getMessage(String message) {
		Map<String, String> result = new HashMap<>();
		if (message.equals("update_success")) {
			result.put("message", "cập nhật thành công !!!");
			result.put("alert", "success");
		} else if (message.equals("insert_success")) {
			result.put("message", "thêm mới thành công !!!");
			result.put("alert", "success");
		} else if (message.equals("delete_success")) {
			result.put("message", "xóa thành công !!!");
			result.put("alert", "success");
		} else if (message.equals("error_system")) {
			result.put("message", "Lỗi hệ thống");
			result.put("alert", "danger");
		}
		return result;
	}

}
