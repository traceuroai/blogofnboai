package com.blog.dto;

import lombok.Data;

@Data
public class NewDTO extends AbstractDTO<NewDTO>{

	private String title;
	private String thumbnail;
	private String shortDescription;
	private String content;
	private Long categoryId;
	private String categoryCode;
	private int likes;
	private int views;
	
}
