package com.blog.dto;

public class UploadFileDTO {
	
	private Long id;
	private String base64;// chứa dữ liệu hình
	private String name;
	
	public String getBase64() {
		return base64.split(",")[1];
	}
	
	public void setBase64(String base64) {
		this.base64 = base64;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	
}
