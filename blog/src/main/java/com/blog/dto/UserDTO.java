package com.blog.dto;

import java.util.List;

import lombok.Data;

@Data
public class UserDTO extends AbstractDTO<UserDTO>{

	private String userName;
	private String fullName;
	private String password;
	private int status;
	private String phone;
	private List<RoleDTO> roles;
	
}
