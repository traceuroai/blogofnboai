package com.blog.api.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.blog.dto.CategoryDTO;
import com.blog.service.ICategoryService;

@RestController
public class CategoryAPI {
	
	@Autowired
	private ICategoryService categoryService;

	@PostMapping(value = "/admin67/category-api")
	public CategoryDTO createCategory (@RequestBody CategoryDTO categoryDTO)
	{
		return categoryService.save(categoryDTO);
	}
	
	@PutMapping(value = "/admin67/category-api")
	public CategoryDTO updateCategory(@RequestBody CategoryDTO categoryDTO)
	{
		return categoryService.update(categoryDTO);
	}
	
	@DeleteMapping(value = "/admin67/category-api")
	public void deleteNew(@RequestBody long[] ids)
	{
		categoryService.delete(ids);
	}
	
}
