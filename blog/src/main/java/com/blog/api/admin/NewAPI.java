package com.blog.api.admin;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.blog.dto.NewDTO;
import com.blog.dto.UploadFileDTO;
import com.blog.service.INewService;
import com.blog.util.uploadFileUtils;

@RestController(value = "newApiOfAdmin")
public class NewAPI {

	@Autowired
	private INewService newService;
	
	@Autowired
	private uploadFileUtils uploadFiles;
	
	@PostMapping(value = "/admin67/new-api")
	public NewDTO createNew (@RequestBody NewDTO newDTO)
	{
//		UploadFileDTO file = new UploadFileDTO();
//		String path = null;
//		try {
//            byte[] decodeBase64 = Base64.getDecoder().decode(uploadFileDTO.getBase64().getBytes());
//            path = uploadFiles.writeOrUpdate(decodeBase64,"/thumbnail/" + uploadFileDTO.getName());//  File.separator + uploadFileDTO.getName()
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//		file.setName(path);
//		newDTO.setThumbnail(path);
		return newService.save(newDTO);
	}
	
	@PostMapping(value = "/admin67/new-api-image")
	public void upload (@RequestBody UploadFileDTO uploadFileDTO)
	{
		UploadFileDTO file = new UploadFileDTO();
		file.setId(uploadFileDTO.getId());
		String path = null;
		try {
            byte[] decodeBase64 = Base64.getDecoder().decode(uploadFileDTO.getBase64().getBytes());
            path = uploadFiles.writeOrUpdate(decodeBase64,"/thumbnail/" + uploadFileDTO.getName());//  File.separator + uploadFileDTO.getName()
        } catch (Exception e) {
            e.printStackTrace();
        }
		file.setName(path + "/" + uploadFileDTO.getName());
		saveThumbnail(file);
	}
	public void saveThumbnail(UploadFileDTO file)
	{
		NewDTO newdto = newService.findOneById(file.getId());
		newdto.setThumbnail(file.getName());
		newService.save(newdto);
	}
	
	@PutMapping(value = "/admin67/new-api")
	public NewDTO updateNew(@RequestBody NewDTO newDTO)
	{
		return newService.save(newDTO);
	}
	
	@DeleteMapping(value = "/admin67/new-api")
	public void deleteNew(@RequestBody long[] ids)
	{
		newService.deleteNew(ids);
	}
	
}
