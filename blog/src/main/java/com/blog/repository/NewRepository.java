package com.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blog.entity.NewEntity;

public interface NewRepository extends JpaRepository<NewEntity, Long>{
	
	NewEntity findOneById(long id);
	
	List<NewEntity> findByCategoryCode(String code);

}
