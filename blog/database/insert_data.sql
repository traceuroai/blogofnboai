use blogitspringmvc;

insert into role (code, name) values ('ADMIN', 'admin');
insert into role (code, name) values ('USER', 'user');

insert into user (username, fullname, password, phone, status) values ('oaiker2k@gmail.com', 'admin', '$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG', '0989739956', 1);
insert into user (username, fullname, password, phone, status) values ('oai@gmail.com', 'ba oai', '$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG', '0989739956', 1);
insert into user (username, fullname, password, phone, status) values ('a@gmail.com', 'nguyen ba a', '$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG', '0989739956', 0);
insert into user (username, fullname, password, phone, status) values ('b@gmail.com', 'nguyen ba b', '$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG', '0989739956', 1);

insert into user_role(userid, roleid) values (1,1);
insert into user_role(userid, roleid) values (2,2);
insert into user_role(userid, roleid) values (3,2);
insert into user_role(userid, roleid) values (4,2);

INSERT INTO `blogitspringmvc`.`new` (`content`, `likes`, `shotdescription`, `title`, `views`, `category_id`) VALUES ('test1', '1', 'test1', 'test1', '1', '3');